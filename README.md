# README #

# Copyright
These scripts etc. can be freely used in Gluster, glusterfs and/or NFS-ganesha
and or their derivatives using their relevant copyright licenses... hope somebody would find them helpful ;)


### What is this repository for? ###

* Quick summary

Not that this is an Application, BTW, but my scripts to get GlusterFS + NFS + HA going on Debian8.7 VMs on a MacOSX with Parallels Desktop.

* Version 0.0.0.pre-alpha

### How do I get set up? ###

* Summary of set up

 Debian VM template that have the debian user and using the Parallels Desktop tools installed with the needed ssh keys pusehd to debian user, and then, the *important part* you need *sudo* working for debian user... there are troubles with Ansible and SU

* Configuration

I have a Host-only network for the VMs, and that is renamed to "gluster" using prlctl

* Dependencies
* Deployment instructions

the set-vms.sh create the VMs and then you run ```debops playbooks/gluster.yml  -f 20 -vv```


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
