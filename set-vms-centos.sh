#!/bin/bash

# set -x
ClientList="c1 c2"
GList="ds1 ds2 ds3"
VMList="$ClientList $GList"

for i in $VMList
do
	(
		prlctl stop $i --kill
		prlctl delete $i
	)&
done

wait

for i in $ClientList
do
	prlctl clone Debian8netinst-tmpl --name $i --linked
done

for i in $GList
do
	prlctl clone ed86106a-256e-44cb-85e0-d9127a70b815 --name $i --linked
done

for i in $VMList
do
	for n in net0 net1
	do
		prlctl set $i --device-set $n --mac `jq -r ".[].Hardware.${n}.mac" $i.prl`
	done
done

for i in ds1 ds2
do
	prlctl set $i --device-add hdd --iface sata --position 2 --size `expr 100 \\* 1024` --enable &
done

for i in ds3
do
	prlctl set $i --device-add hdd --iface sata --position 2 --size `expr 10 \\* 1024` --enable  &
done

wait

for i in $VMList
do
	prlctl start $i
	# sleep 1
done
#
# for i in $VMList
# do
# 	prlctl suspend $i
# 	sleep 0.5
# 	prlctl resume $i
# 	sleep 1
# done
#

for i in $VMList
do
	while [ `(prlctl list -fj $i | jq -r ".[].ip_configured") | grep -c -- -` -gt 0 ]
	do
		sleep 5; echo waiting $i; date
	done
done


for i in $VMList
do
	prlctl suspend $i
	prlctl resume $i
	sleep 0.5
done
for i in $VMList
do
	prlctl exec $i /sbin/reboot
done

for i in $VMList
do
	echo checking $i
	while [ `(prlctl list -fj $i | jq -r ".[].ip_configured") | grep -c -- -` -gt 0 ]
	do
		sleep 5; echo waiting $i; date
	done
done
sleep 5
