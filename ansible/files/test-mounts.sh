#!/bin/bash
# Copyright: The same as Gluster & NFS-ganesha when used in and with them

set -x

gms=`gluster volume list`
fms=`systemctl --failed | awk '/mount/ {print $2}'`
for ms in $fms
do
 what=`systemctl status $ms|awk '/What:/ {print $2}'`
 mcmd=`systemctl status $ms|awk '/Process:/ {print $0}'`
 gfs=`systemctl status $ms|awk '/Process:/ {print $0}'| grep -c glusterfs`
 if [ $gfs -gt 0 ]
 then
  if systemctl restart $ms
  then
		#At this point, glusterfs's are *all* retried by SystemD WT..!!!!!
		#So that's why I let this one exit, doesn't help to retry the restart
		# BUT this is a problem also when this properly failed and.... I'll need to
		# add/test something here...
	  exit 255
	else
		#Now we need to check the whether the path haven't been mounted before O_o
		exit 1
		#For now I'm not going to follw this
	fi
 fi
done

# If we've reached this point, there wasn't a failed mountpoint
# BUT we need to check for whether this haven't been called before the attempts
# to the filesystem mounts, thus we need to check whether there is a glusterfs
# in fstab and aren't mountedmount

#Okay, here is a "sleeping" trouble that hits us, as the fuse mount gets "mounted"
#But then, when the quorum/etc. isn't fulfilled, the mounts are then unmount'ed
# WT.!?!?!?

sleep 10
#In the logs I've seen ~4-5secs between the initial mount/start and the unmount

#grep glusterfs /etc/fstab | awk
gfc=`sed -e 's/#.$//' </etc/fstab|grep -c glusterfs`
gfm=`grep -i glusterfs /proc/mounts|wc -l`

if [ $gfm -lt $gfc ]
then
	sleep 5
	exit 200
fi
